//
//  ViewController.m
//  UIView IBOutlets
//
//  Created by Timofei Harhun on 16.03.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.black.frame = CGRectMake(0, self.view.frame.size.height/2-self.view.frame.size.width/2, self.view.frame.size.width, self.view.frame.size.width);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat) randOfRangeZeroOne {
    CGFloat rand = arc4random()%256/255.f;
    return rand;
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIColor*) changeColor {
    
    return [[UIColor alloc] initWithRed:[self randOfRangeZeroOne]
                                  green:[self randOfRangeZeroOne]
                                   blue:[self randOfRangeZeroOne]
                                  alpha:1];

}

- (void) mixSqaresChess {
    NSMutableArray* mass =[[NSMutableArray alloc] init];
    [mass addObject:[NSNumber numberWithInteger:self.chess.count+1]];
    
    NSInteger(^block)()=^NSInteger{
        int a=0;
        NSInteger rand = arc4random()%self.chess.count;
        do{
            NSNumber* num = mass[a];
            a++;
            if ([num integerValue] == rand) {
                rand = arc4random()%self.chess.count;
                a=0;
            }
            if (a==mass.count) {
                [mass addObject:[NSNumber numberWithInteger:rand]];
                break;
            }
        }while(1);
        return rand;
    };
    
    [UIView animateWithDuration:1 animations:^{
        
        for (int i=0; i<12; i++) {
            NSInteger   one = block(),
                        two = block();
            UIView* viewOne = self.chess[one];
            UIView* viewTwo = self.chess[two];
            CGRect rect = viewOne.frame;
            viewOne.frame = viewTwo.frame;
            viewTwo.frame = rect;
        }
    }];
    
}

- (void) willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    UIColor* color = [self changeColor];
    
    [UIView animateWithDuration:1 animations:^{
        for (UIView* view in self.blackSuaqre) {
            view.backgroundColor = color;
        }
    }];
    
    [self mixSqaresChess];
    
}

@end
