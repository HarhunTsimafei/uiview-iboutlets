//
//  ViewController.h
//  UIView IBOutlets
//
//  Created by Timofei Harhun on 16.03.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *blackSuaqre;

@property (weak, nonatomic) IBOutlet UIView *black;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *chess;

@end

